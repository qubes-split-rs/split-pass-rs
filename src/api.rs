//! # API
//!
//! Types describing the exchanged messages.

use std::io::{Cursor, Read, Write};

use anyhow::{anyhow, Context, Result};
use prost::Message;

use crate::api::response::Body;

include!(concat!(env!("OUT_DIR"), "/api.rs"));

impl Request {
    /// Create new request
    pub fn new(name: String) -> Self {
        Self { name }
    }

    /// Create new request with encoded data read from reader.
    ///
    /// Use when reading from [std::io::stdin()] or the stdout pipe of another process.
    ///
    /// ```
    /// use qubes_split_rs::api::Request;
    /// let mut buf: Vec<u8> = vec![5, 10, 3, 98, 97, 114];
    /// let response = Request::read(&mut buf.as_slice()).unwrap();
    /// assert_eq!(response.name, "bar");
    /// ```
    pub fn read(reader: &mut impl Read) -> Result<Self> {
        read::<Self>(reader)
    }

    /// Write encoded request to writer.
    ///
    /// Use when writing to [std::io::stdout()] or the stdin pipe of another process.
    ///
    /// ```
    /// use qubes_split_rs::api::Request;
    /// let mut buf: Vec<u8> = Vec::new();
    /// let request = Request::new("foo".to_string());
    /// request.write(&mut buf).unwrap();
    /// assert_eq!(buf, vec![5, 10, 3, 102, 111, 111]);
    /// ```
    pub fn write(self, writer: &mut impl Write) -> Result<()> {
        write(writer, self)
    }
}

impl Response {
    /// Create new response
    pub fn new(body: Body) -> Self {
        Self { body: Some(body) }
    }

    /// Create new response with encoded data read from reader.
    ///
    /// Use when reading from [std::io::stdin()] or the stdout pipe of another process.
    ///
    /// ```
    /// use qubes_split_rs::api::Response;
    /// use qubes_split_rs::api::response::Body::Greeting;
    ///
    /// let mut buf: Vec<u8> = vec![7, 10, 5, 10, 3, 98, 97, 114];
    /// let response = Response::read(&mut buf.as_slice()).unwrap();
    /// match response.body.expect("got empty body") {
    ///   Greeting(greeting) => assert_eq!(greeting.message, "bar"),
    ///   _ => panic!("expected Greeting!"),
    /// }
    /// ```
    pub fn read(reader: &mut impl Read) -> Result<Self> {
        read::<Self>(reader)
    }

    /// Write encoded response to writer.
    ///
    /// Use when writing to [std::io::stdout()] or the stdin pipe of another process.
    ///
    /// ```
    /// use qubes_split_rs::api::{Response,Greeting};
    /// let mut buf: Vec<u8> = Vec::new();
    /// let response = Response::new(Greeting::body("bar".to_string()));
    /// response.write(&mut buf).unwrap();
    /// assert_eq!(buf, vec![7, 10, 5, 10, 3, 98, 97, 114]);
    /// ```
    pub fn write(self, writer: &mut impl Write) -> Result<()> {
        write(writer, self)
    }
}

impl Greeting {
    pub fn body(message: String) -> Body {
        Body::Greeting(Greeting { message })
    }
}

impl Insult {
    pub fn body(message: String) -> Body {
        Body::Insult(Insult { message })
    }
}

impl Error {
    pub fn body(message: String) -> Body {
        Body::Error(Error { message })
    }
}

fn read<T: Message + Default>(reader: &mut impl Read) -> Result<T> {
    let mut buf = [0u8; 1];
    reader
        .read_exact(&mut buf)
        .with_context(|| "failed to determine message length")?;
    let n = buf[0] as usize;
    let mut buf = Vec::with_capacity(n);
    let nn = reader
        .take(n as u64)
        .read_to_end(&mut buf)
        .with_context(|| "failed to read message")?;
    if nn != n {
        return Err(anyhow!("failed to read message"));
    }
    T::decode(&mut Cursor::new(buf)).with_context(|| "failed to decode message")
}

fn write(writer: &mut impl Write, message: impl Message) -> Result<()> {
    let buf = message.encode_length_delimited_to_vec();
    writer
        .write_all(&buf)
        .with_context(|| "failed to encode message")
}

#[cfg(test)]
mod tests {
    use crate::api::response::Body;
    use crate::api::Response;

    #[test]
    fn read_missing_data() {
        let buf: Vec<u8> = vec![5];
        match Response::read(&mut buf.as_slice()) {
            Err(error) => assert_eq!(error.to_string(), "failed to read message"),
            _ => panic!("expected error"),
        }
    }

    #[test]
    fn read_excess_data() {
        let buf: Vec<u8> = vec![7, 10, 5, 10, 3, 98, 97, 114, 42];
        let response = Response::read(&mut buf.as_slice()).unwrap();
        match response.body.expect("got empty body") {
            Body::Greeting(greeting) => assert_eq!(greeting.message, "bar"),
            _ => panic!("expected Greeting!"),
        }
    }

    #[test]
    fn read_corrupt_data() {
        let buf: Vec<u8> = vec![5, 11, 3, 98, 97, 114, 42];
        match Response::read(&mut buf.as_slice()) {
            Err(error) => assert_eq!(error.to_string(), "failed to decode message"),
            _ => panic!("expected error"),
        }
    }
}
