//! Helper connect with the server and process requests and responses.

use std::process::{Child, Command, Stdio};

use anyhow::{anyhow, Context, Result};

use crate::api::response::Body;
use crate::api::*;

pub mod api;

/// Connect to the server
///
/// This is intended to connect to [`qrexec-client-vm`](https://www.qubes-os.org/doc/qrexec/)
/// as the server. `qrexec-client-vm` takes the target vm and a service
/// name as its arguments. The data between the client and the server
/// is exchanged through STDIN and STDOUT.
///
/// ```
/// use std::path::PathBuf;
/// use std::io::{Read, Write};
/// use qubes_split_rs::connect;
/// let program = format!("{}/test/dummy.sh", env!("CARGO_MANIFEST_DIR"));
/// let server = connect(&program, &"my-target", &"my-service", &Some("my-argument".to_string())).unwrap();
/// let _ = server.stdin.unwrap().write_all(&"foo".as_bytes());
/// let mut buf: Vec<u8> = Vec::new();
/// let _ = server.stdout.unwrap().read_to_end(&mut buf);
/// let expected = "target: my-target\nservice: my-service+my-argument\nstdin: foo\n";
/// assert_eq!(expected, std::str::from_utf8(&buf).unwrap());
/// ```
pub fn connect(
    program: &str,
    target: &str,
    service: &str,
    argument: &Option<String>,
) -> Result<Child> {
    let service = match argument {
        Some(arg) => format!("{}+{}", service, arg),
        None => service.to_string(),
    };
    Command::new(program)
        .arg(target)
        .arg(service)
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .with_context(|| "failed to call server")
}

/// Process a request
///
/// ```
/// use qubes_split_rs::api::Request;
/// use qubes_split_rs::api::response::Body;
/// use qubes_split_rs::process_request;
/// let mut buf: Vec<u8> = vec![5, 10, 3, 98, 97, 114];
/// let request = Request::read(&mut buf.as_slice());
/// match process_request(request, "greeting").body.expect("got empty body") {
///   Body::Greeting(greeting) => assert_eq!(greeting.message, "Hello, bar!"),
///   _ => panic!("expected greeting"),
/// }
/// ```
pub fn process_request(request: Result<Request>, argument: &str) -> Response {
    let body = match request {
        Err(error) => Error::body(format!("your bytes must have been muddled up: {}", error)),
        Ok(request) => match argument {
            "greeting" => Greeting::body(format!("Hello, {}!", request.name)),
            "insult" => Insult::body(
                "I smell something burning. Are you trying to think again?".to_string(),
            ),
            _ => Error::body("I do not get what you want from me!".to_string()),
        },
    };

    Response::new(body)
}

/// Process a response
///
/// ```
/// use qubes_split_rs::api::Response;
/// use qubes_split_rs::process_response;
/// let mut buf: Vec<u8> = vec![7, 10, 5, 10, 3, 98, 97, 114];
/// let response = Response::read(&mut buf.as_slice());
/// let output = process_response(response).expect("failed to process");
/// assert_eq!(output, "bar");
/// ```
pub fn process_response(response: Result<Response>) -> Result<String> {
    let response = response.with_context(|| "failed to receive response from server")?;
    let body = response
        .body
        .with_context(|| "server responded with an empty message")?;
    match body {
        Body::Greeting(greeting) => Ok(greeting.message),
        Body::Insult(insult) => Ok(format!("You have been insulted: {}", insult.message)),
        Body::Error(error) => Err(anyhow!(error.message).context("received error from server")),
    }
}
