use std::io::{stdin, stdout};

use anyhow::{Context, Result};

use clap::{Parser, Subcommand};

use qubes_split_rs::api::*;
use qubes_split_rs::*;

#[derive(Parser)]
#[command(author, version, about)]
struct Cli {
    #[command(subcommand)]
    command: Command,
}

#[derive(Subcommand)]
enum Command {
    /// Run the server
    Server {
        /// The service argument
        #[arg(default_value = "greeting")]
        argument: String,
    },
    /// Run the client
    Client {
        /// Path to the qrexec client.
        #[arg(short, long, default_value = "qrexec-client-vm")]
        program: String,

        /// Name of the Qubes RPC service.
        #[arg(short, long, default_value = "qubes-split-rs.Hello")]
        service: String,

        /// The service argument [possible values: greeting, insult, …]
        #[arg(short, long)]
        argument: Option<String>,

        /// Name of the target qube running the Qubes RPC service.
        target: String,

        /// Your name
        name: String,
    },
}

fn main() -> Result<()> {
    let cli = Cli::parse();

    match cli.command {
        Command::Server { argument } => {
            let request = Request::read(&mut stdin());
            let response = process_request(request, &argument);
            response
                .write(&mut stdout())
                .with_context(|| "failed to write response")
        }

        Command::Client {
            program,
            name,
            target,
            argument,
            service,
        } => {
            let server = connect(&program, &target, &service, &argument)?;

            let request = Request::new(name);
            let mut stdin = server
                .stdin
                .with_context(|| "failed to connect to server")?;
            request
                .write(&mut stdin)
                .with_context(|| "failed to send request to server")?;

            let mut stdout = server
                .stdout
                .with_context(|| "failed to read from server")?;
            let response = Response::read(&mut stdout);
            println!("{}", process_response(response)?);
            Ok(())
        }
    }
}
