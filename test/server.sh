#!/bin/sh

argument=""

case "$2" in
  *"+"*)
    argument="$(echo "$2" | cut -d"+" -f2)"
  ;;
esac

exec target/debug/qubes-split-rs server $argument
