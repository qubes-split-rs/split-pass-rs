# QubesOS Split Password Management

`split-pass-rs` is a password management helper to be used in the context [QubesOS][qubesos].
It allows to securely and safely access passwords stored in one [qube][qube] (a vault) from another qube.
It is built upon the idea of [Split GPG][split-gpg] adapted to the needs of passwords.

## Features

* [ ] Passwords can be accessed from any password manager
* [ ] Command line interface to access passwords
* [ ] Browser plugin to access passwords
* [ ] Access of different qubes can be restricted to only a subset of passwords
* [ ] Works out of the box with [KeepassXC][keepassxc]
* [ ] Works out of the box with [pass][pass]

[keepassxc]: https://keepassxc.org/
[pass]: https://www.passwordstore.org/
[qube]: https://www.qubes-os.org/doc/glossary/#qube
[qubesos]: https://www.qubes-os.org/
[split-gpg]: https://www.qubes-os.org/doc/split-gpg/

